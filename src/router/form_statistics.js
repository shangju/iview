/**
 * 报表统计
 */
export default [
    {
        name: 'form_statistics_user',
        path: '/form_statistics/user',//用户统计
        component: resolve => require(['@/views/form_statistics/user/list.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'form_statistics_user_detail',
                path: '/form_statistics/user/detail/:id',
                component: resolve => require(['@/views/form_statistics/user/detail.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/user'},
            },
            {
                name: 'form_statistics_user_stock',
                path: '/form_statistics/user/stock/:id',
                component: resolve => require(['@/views/common/stocklist.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/user'},
            }
        ]
    },
    {
        name: 'form_statistics_day_entrust',
        path: '/form_statistics/day_entrust',//当日委托
        component: resolve => require(['@/views/form_statistics/day_entrust/list.vue'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'form_statistics_history_entrust',
        path: '/form_statistics/history_entrust',//历史委托
        component: resolve => require(['@/views/form_statistics/history_entrust/list.vue'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'form_statistics_day_order',
        path: '/form_statistics/day_order',//当日买入、卖出
        redirect: '/form_statistics/day_order/buy',
        component: resolve => require(['@/views/form_statistics/day_order/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'form_statistics_day_order_buy',
                path: '/form_statistics/day_order/buy',
                component: resolve => require(['@/views/form_statistics/day_order/buy.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/day_order'},
            },
            {
                name: 'form_statistics_day_order_sold',
                path: '/form_statistics/day_order/sold',
                component: resolve => require(['@/views/form_statistics/day_order/sold.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/day_order'},
            }
        ]
    },
    {
        name: 'form_statistics_history_order',
        path: '/form_statistics/history_order',//历史买入、卖出
        redirect: '/form_statistics/history_order/buy',
        component: resolve => require(['@/views/form_statistics/history_order/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'form_statistics_history_order_buy',
                path: '/form_statistics/history_order/buy',
                component: resolve => require(['@/views/form_statistics/history_order/buy.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/history_order'},
            },
            {
                name: 'form_statistics_history_order_sold',
                path: '/form_statistics/history_order/sold',
                component: resolve => require(['@/views/form_statistics/history_order/sold.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/history_order'},
            }
        ]
    },
    {
        name: 'form_statistics_hold_position',
        path: '/form_statistics/hold_position',//持仓除权、日志
        redirect: '/form_statistics/hold_position/ex_right',
        component: resolve => require(['@/views/form_statistics/hold_position/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'form_statistics_hold_position_ex_right',
                path: '/form_statistics/hold_position/ex_right',
                component: resolve => require(['@/views/form_statistics/hold_position/ex_right.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/hold_position'},
            },
            {
                name: 'form_statistics_hold_position_log',
                path: '/form_statistics/hold_position/log',
                component: resolve => require(['@/views/form_statistics/hold_position/log.vue'], resolve),
                meta: {model: 'Project', parent: '/form_statistics/hold_position'},
            }
        ]
    },
    {
        name: 'form_statistics_risk',
        path: '/form_statistics/risk',//风控日志
        component: resolve => require(['@/views/form_statistics/risk/list.vue'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'form_statistics_funding',
        path: '/form_statistics/funding',//配资峰值
        component: resolve => require(['@/views/form_statistics/funding/list.vue'], resolve),
        meta: {model: 'Project'},
    },
];






















