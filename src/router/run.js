/**
 * 运营统计
 */
export default [
    {
        name: 'run_total',//运营总报
        path: '/run/total',
        component: resolve => require(['@/views/run/total'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'run_daily',//运营日报
        path: '/run/daily',
        component: resolve => require(['@/views/run/daily'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'run_agent_benefit',//代理收益
        path: '/run/agent_benefit',
        component: resolve => require(['@/views/run/agent_benefit'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'run_err',//交易异常处理
        path: '/run/err',
        component: resolve => require(['@/views/run/err'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'run_game',//炒股大赛
        path: '/run/game',
        component: resolve => require(['@/views/run/game'], resolve),
        meta: {model: 'Project'},
    },
];

















