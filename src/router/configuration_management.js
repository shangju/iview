/**
 * 配资管理
 */
export default [
    {
        name: 'configuration_management',
        path: '/configuration_management/interest',//配资利息
        component: resolve => require(['@/views/configuration_management/interest/list.vue'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'configuration_management_log',
        path: '/configuration_management/log',//配资记录
        component: resolve => require(['@/views/configuration_management/log/list.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'configuration_management_log',
                path: '/configuration_management_log/stock/:log_id',
                component: resolve => require(['@/views/common/stocklist.vue'], resolve),
                meta: {model: 'Project', parent: '/configuration_management/log'},
            }
        ]
    },
    {
        name: 'configuration_management_assess',
        path: '/configuration_management/assess',//风险评估
        component: resolve => require(['@/views/configuration_management/risk_assessment/list.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'configuration_management_assess',
                path: '/configuration_management_assess/stock/:log_id',
                component: resolve => require(['@/views/common/stocklist.vue'], resolve),
                meta: {model: 'Project', parent: '/configuration_management/assess'},
            }
        ]
    },
    {
        name: 'configuration_management_allocation',
        path: '/configuration_management/allocation',//配资分配
        component: resolve => require(['@/views/configuration_management/allocation/list.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'configuration_management_allocation',
                path: '/configuration_management_allocation/stock/:log_id',
                component: resolve => require(['@/views/common/stocklist.vue'], resolve),
                meta: {model: 'Project', parent: '/configuration_management/allocation'},
            }
        ]
    },
    {
        name: 'configuration_management_financial_details',
        path: '/configuration_management/financial_details',//配资资金明细
        component: resolve => require(['@/views/configuration_management/financial_details/list.vue'], resolve),
        meta: {model: 'Project'},
    },
];