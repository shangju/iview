/**
 * 资金管理
 */
export default [
    {
        name: 'fundsmanage_manual',
        path: '/fundsmanage/manual',//手动入款、扣款
        component: resolve => require(['@/views/fundsmanage/manual/form.vue'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'fundsmanage_recharge',
        path: '/fundsmanage/recharge',//充值
        redirect: '/fundsmanage/recharge/manage',
        component: resolve => require(['@/views/fundsmanage/recharge/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'fundsmanage_recharge_manage',
                path: '/fundsmanage/recharge/manage',
                component: resolve => require(['@/views/fundsmanage/recharge/manage.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/recharge'},
            },
            {
                name: 'fundsmanage_recharge_total',
                path: '/fundsmanage/recharge/total',
                component: resolve => require(['@/views/fundsmanage/recharge/total.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/recharge'},
            }
        ]
    },
    {
        name: 'fundsmanage_user',
        path: '/fundsmanage/user',//用户
        redirect: '/fundsmanage/user/deposit',
        component: resolve => require(['@/views/fundsmanage/user/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'fundsmanage_user_deposit',
                path: '/fundsmanage/user/deposit',
                component: resolve => require(['@/views/fundsmanage/user/deposit.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/user'},
            },
            {
                name: 'fundsmanage_user_total',
                path: '/fundsmanage/user/total',
                component: resolve => require(['@/views/fundsmanage/user/total.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/user'},
            }
        ]
    },
    {
        name: 'fundsmanage_agency',
        path: '/fundsmanage/agency',//代理
        redirect: '/fundsmanage/agency/deposit',
        component: resolve => require(['@/views/fundsmanage/agency/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'fundsmanage_agency_deposit',
                path: '/fundsmanage/agency/deposit',
                component: resolve => require(['@/views/fundsmanage/agency/deposit.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/agency'},
            },
            {
                name: 'fundsmanage_agency_total',
                path: '/fundsmanage/agency/total',
                component: resolve => require(['@/views/fundsmanage/agency/total.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/agency'},
            }
        ]
    },
    {
        name: 'fundsmanage_earning',
        path: '/fundsmanage/earning',//推广
        component: resolve => require(['@/views/fundsmanage/earning/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'fundsmanage_earning_edit',
                path: '/fundsmanage/earning/edit/:id',
                component: resolve => require(['@/views/fundsmanage/manual/form.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/earning'},
            }
        ]
    },
    {
        name: 'fundsmanage_detail',
        path: '/fundsmanage/detail',//资金明细
        component: resolve => require(['@/views/fundsmanage/details/list.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'fundsmanage_detail_edit',
                path: '/fundsmanage/detail/edit/:id',
                component: resolve => require(['@/views/fundsmanage/manual/form.vue'], resolve),
                meta: {model: 'Project', parent: '/fundsmanage/detail'},
            }
        ]
    },
];






















