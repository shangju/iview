/**
 * 内容管理
 */
export default [
    {
        name: 'content_manage',
        path: '/content',//手动入款、扣款
        redirect: '/content/list',
        component: resolve => require(['@/views/content/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'content_list',
                path: '/content/list',
                component: resolve => require(['@/views/content/list.vue'], resolve),
                meta: {model: 'Project', parent: '/content'},
            },
            {
                name: 'content_add',
                path: '/content/add/:id',
                component: resolve => require(['@/views/content/add.vue'], resolve),
                meta: {model: 'Project', parent: '/content'},
            },
        ]
    },
];






















