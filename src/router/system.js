/**
 * 系统
 */
export default [
    {
        name: 'system_setting_base',
        path: '/system/setting_base',
        component: resolve => require(['@/views/system/setting/base/system-setting-base'], resolve),
        meta: {model: 'Project'}
    },
    {
        name: 'system_auth_group_list',
        path: '/system/auth_group/list',
        component: resolve => require(['@/views/system/auth/auth-group-list'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'auth_group_add',
                path: '/system/auth_group/add',
                component: resolve => require(['@/views/system/auth/auth_group_add'], resolve),
                meta: {model: 'Project', parent: '/system/auth_group/list'},
            },
            {
                name: 'auth_group_edit',
                path: '/system/auth_group/edit/:id',
                component: resolve => require(['@/views/system/auth/auth_group_edit'], resolve),
                meta: {model: 'Project', parent: '/system/auth_group/list'},
            },
            {
                name: 'system_group_user_list',
                path: '/system/auth_group/user_list/:id',
                component: resolve => require(['@/views/system/auth/group-user-list'], resolve),
                meta: {model: 'Project', parent: '/system/auth_group/list'}
            },
        ]
    },
    {
        name: 'system_menu_model_list',
        path: '/system/menu_model/list',
        component: resolve => require(['@/views/system/auth/menu-model-list'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'menu_model_add',
                path: '/system/menu_model/add',
                component: resolve => require(['@/views/system/auth/menu-model-add'], resolve),
                meta: {model: 'Project', parent: '/system/menu_model/list'},
            },
            {
                name: 'menu_model_edit',
                path: '/system/menu_model/edit/:id',
                component: resolve => require(['@/views/system/auth/menu-model-edit'], resolve),
                meta: {model: 'Project', parent: '/system/menu_model/list'},
            }
        ]
    },
    {
        name: 'system_auth_menu_list',
        path: '/system/auth_menu/list',
        component: resolve => require(['@/views/system/auth/auth-menu-list'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'auth_menu_add',
                path: '/system/auth_menu/add',
                component: resolve => require(['@/views/system/auth/auth-menu-add'], resolve),
                meta: {model: 'Project', parent: '/system/auth_menu/list'},
            },
            {
                name: 'auth_menu_edit',
                path: '/system/auth_menu/edit/:id',
                component: resolve => require(['@/views/system/auth/auth-menu-edit'], resolve),
                meta: {model: 'Project', parent: '/system/auth_menu/list'},
            }
        ]
    },
    {
        name: 'system_auth_rule_list',
        path: '/system/auth_rule/list',
        component: resolve => require(['@/views/system/auth/auth-rule-list'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'auth_rule_add',
                path: '/system/auth_rule/add',
                component: resolve => require(['@/views/system/auth/auth-rule-add'], resolve),
                meta: {model: 'Project', parent: '/system/auth_rule/list'},
            },
            {
                name: 'auth_rule_edit',
                path: '/system/auth_rule/edit/:id',
                component: resolve => require(['@/views/system/auth/auth-rule-edit'], resolve),
                meta: {model: 'Project', parent: '/system/auth_rule/list'},
            }
        ]
    },
    {
        name: 'system_log_list',
        path: '/system/log/list',
        component: resolve => require(['@/views/system/log/log-list'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'system_closed',
        path: '/system/closed',//休市安排 | 新增
        redirect: '/system/closed/list',
        component: resolve => require(['@/views/system/closed/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'system_closed_list',
                path: '/system/closed/list',
                component: resolve => require(['@/views/system/closed/list.vue'], resolve),
                meta: {model: 'Project', parent: '/system/closed'},
            },
        ]
    },
    {
        name: 'system_stock',
        path: '/system/stock',//股票管理 | 新增
        redirect: '/system/stock/list',
        component: resolve => require(['@/views/system/stock/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'system_stock_list',
                path: '/system/stock/list',
                component: resolve => require(['@/views/system/stock/list.vue'], resolve),
                meta: {model: 'Project', parent: '/system/closed'},
            },
            {
                name: 'system_stock_add',
                path: '/system/stock/add/:id',
                component: resolve => require(['@/views/system/stock/add.vue'], resolve),
                meta: {model: 'Project', parent: '/system/closed'},
            },
            {
                name: 'system_stock_edit',
                path: '/system/stock/edit/:id',
                component: resolve => require(['@/views/system/stock/edit.vue'], resolve),
                meta: {model: 'Project', parent: '/system/closed'},
            },
        ]
    },
    {
        name: 'system_new_share',//检查新股
        path: '/system/new_share',
        component: resolve => require(['@/views/system/new_share/index'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'system_rules',
        path: '/system/rules',//规则验证
        redirect: '/system/rules/open_close',
        component: resolve => require(['@/views/system/rules/index.vue'], resolve),
        meta: {model: 'Project'},
        children: [
            {
                name: 'system_rules_open_close',//开市和收市时间设定
                path: '/system/rules/open_close',
                component: resolve => require(['@/views/system/rules/open_close.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_exit',//平仓时间设置
                path: '/system/rules/exit',
                component: resolve => require(['@/views/system/rules/exit.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_withdrawal',//提款时间设置
                path: '/system/rules/withdrawal',
                component: resolve => require(['@/views/system/rules/withdrawal.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_appfile',//APP文件
                path: '/system/rules/appfile',
                component: resolve => require(['@/views/system/rules/appfile.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_gathering',//收款设置
                path: '/system/rules/gathering',
                component: resolve => require(['@/views/system/rules/gathering.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_baseset',//基本设置
                path: '/system/rules/baseset',
                component: resolve => require(['@/views/system/rules/baseset.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_account',//账号设置
                path: '/system/rules/account',
                component: resolve => require(['@/views/system/rules/account.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_costlimit',//费用限制
                path: '/system/rules/costlimit',
                component: resolve => require(['@/views/system/rules/costlimit.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_fund_param',//配资参数设置
                path: '/system/rules/fund_param',
                component: resolve => require(['@/views/system/rules/fund_param.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
            {
                name: 'system_rules_others',//其他设置
                path: '/system/rules/others',
                component: resolve => require(['@/views/system/rules/others.vue'], resolve),
                meta: {model: 'Project', parent: '/system/rules'},
            },
        ]
    },
    {
        name: 'system_msg',//消息管理
        path: '/system/msg',
        component: resolve => require(['@/views/system/msg/index'], resolve),
        meta: {model: 'Project'},
    },
    {
        name: 'system_pwd',//修改密码
        path: '/system/pwd',
        component: resolve => require(['@/views/system/pwd/index'], resolve),
        meta: {model: 'Project'},
    },
];






















